from pygrimoire import Datalog
from typing import Optional


class Database(object):
    def __init__(self, kif_input: str = '', database: Optional[Datalog] = None) -> None:
        if database:
            self.db = database
        else:
            self.db = Datalog()
            self.db.parse(kif_input)
