################################################################################
#
# USAGE:
#
# kif = KIF()
#
# kif.comment("It begins:")
#
# kif.include('/path/to/this_file.kif')
#
# kif.fact('hello', 'world')
#
# bar = kif.variable('bar')
# with kif.rule('foo', bar, 'baz') as r:
#     r('hello', bar)
#     r('foo', bar)
#
# kif.fact('init', ('cell', 2, 2, 'b'))
#
# print(str(kif))
# db = kif.database()
#
################################################################################

from brigadier.datalog import Database
from contextlib import contextmanager
from pygrimoire import Datalog
from typing import Generator, Iterable, List, Union


class IllegalCharacterError(Exception):
    pass


class Var(object):
    def __init__(self, var: str) -> None:
        self.var = '?' + _stringify_term(var)

    def __str__(self) -> str:
        return self.var


# FIXME:  Recursive types are not fully supported as of mypy 0.560
# Term = Union[str, int, Var, Iterable['Term']]
Term = Union[str, int, Var, Iterable]


def _to_ascii(string: str) -> str:
    """ Make sure the unicode string is ascii. """
    try:
        return string.encode('ascii').decode('ascii')
    except UnicodeEncodeError:
        raise IllegalCharacterError()


def _stringify_term(term: Term) -> str:
    """ Convert the given term into an ASCII string.  Raises
    IllegalCharacterError if any string in the term is not a valid datalog
    atom.
    """
    if isinstance(term, Var) or isinstance(term, int):
        return str(term)
    elif isinstance(term, str):
        term = _to_ascii(term)
        for x in term:
            if x.isspace() or x in ('(', ')', ';'):
                raise IllegalCharacterError()
        return term
    else:   # Iterable
        return '(' + ' '.join(_stringify_term(x) for x in term) + ')'


def _blockify(comment: str) -> str:
    """ Turn comment into a block comment. """
    block = ';;\n'
    line = ''
    for word in comment.split():
        if not line and len(word) > 77:
            block += ';; ' + word + '\n'
        elif len(line) + len(word) + 1 > 77:
            block += ';;' + line + '\n'
            line = ''
        else:
            line += ' ' + word
    if line:
        block += ';;' + line + '\n'
    return block + ';;'


class _Literal(object):
    def __init__(self, pred: str, terms: Iterable[Term]) -> None:
        pred = _stringify_term(pred)
        self.terms: List[str] = [pred] + [_stringify_term(x) for x in terms]

    def __str__(self) -> str:
        return '(' + ' '.join(self.terms) + ')'


class Fact(_Literal):
    def __init__(self, pred: str, terms: Iterable[str] = []) -> None:
        super().__init__(pred, terms)


class Rule(object):
    def __init__(self, pred: str, terms: Iterable[Term] = []) -> None:
        self.head = _Literal(pred, terms)
        self.body: List[_Literal] = []

    def __call__(self, pred: str, *terms: Term) -> None:
        self.body.append(_Literal(pred, terms))

    def __str__(self) -> str:
        rule = '(<= ' + str(self.head)
        if len(self.body) == 1:
            rule += ' ' + str(self.body[0])
        elif len(self.body) > 1:
            rule += '\n' + '\n'.join(str(x) for x in self.body)
        return rule + ')'


class KIF(object):
    def __init__(self) -> None:
        self.db = Datalog()
        self.data: List[str] = []

    def __str__(self) -> str:
        return '\n\n'.join(self.data)

    def include(self, path: str) -> str:
        with open(path) as f:
            data = _to_ascii(f.read())
        self.db.parse(data)
        self.data.append(data)
        return data

    def comment(self, comment: str, block: bool = False) -> str:
        comment = _to_ascii(comment)
        comment = _blockify(comment) if block or len(comment) > 78 else '; ' + comment
        self.data.append(comment)
        return comment

    def variable(self, var: str) -> Var:
        return Var(var)

    def variables(self, *variables: str) -> List[Var]:
        return [self.variable(x) for x in variables]

    def fact(self, pred: str, *terms: str) -> Fact:
        fact = Fact(pred, terms)
        string = str(fact)
        self.db.parse(string)
        self.data.append(string)
        return fact

    @contextmanager
    def rule(self, pred: str, *terms: Term) -> Generator[Rule, None, None]:
        rule = Rule(pred, terms)
        yield rule
        string = str(rule)
        self.db.parse(string)
        self.data.append(string)

    def database(self) -> Database:
        return Database(database=self.db)
