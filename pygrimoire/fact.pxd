cdef extern from "fact.h":
    ctypedef struct grm_fact_t:
        pass

    ctypedef struct grm_query_t:
        pass

    void grm_query_free(grm_query_t *query);
