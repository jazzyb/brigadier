cdef extern from "lexer.h":
    ctypedef enum grm_token_type_t:
        GRM_EMPTY_TOKEN
        GRM_OPEN_PAREN
        GRM_CLOSE_PAREN
        GRM_ATOM_LIT
        GRM_VAR_LIT
        GRM_RULE_DEF
        GRM_OR_KW
        GRM_NOT_KW
        GRM_DISTINCT_KW
        GRM_END_PROGRAM

    ctypedef struct grm_token_t:
        grm_token_type_t type
        char *lexeme
        size_t index

cdef extern from "error.h":
    ctypedef enum grm_error_type_t:
        GRM_BODY_SAFETY_ERR
        GRM_EMPTY_PREDICATE_ERR
        GRM_EXTRA_CLOSING_ERR
        GRM_EXTRA_OPENING_ERR
        GRM_HEAD_SAFETY_ERR
        GRM_ILLEGAL_FACT_ERR
        GRM_ILLEGAL_HEAD_ERR
        GRM_ILLEGAL_RULE_DEF_ERR
        GRM_ILLEGAL_VAR_LIT_ERR
        GRM_INVALID_QUERY_ERR
        GRM_KEYWORD_ARITY_ERR
        GRM_KEYWORD_MISUSE_ERR
        GRM_LONE_ATOM_ERR
        GRM_MEMORY_ERR
        GRM_RULE_SYM_MISUSE_ERR
        GRM_UNRESTRICTED_RECURSION_ERR
        GRM_UNSTRATIFIED_RULE_ERR
        GRM_ERROR_LAST

    ctypedef struct grm_error_t:
        int type
        grm_token_t token
        void *user_data

    int grm_error_message(char *dst, size_t size, const char *buffer, grm_error_t *error)
