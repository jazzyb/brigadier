from cpython.mem cimport PyMem_Malloc, PyMem_Free
from pygrimoire.error cimport grm_error_t, grm_error_message
from pygrimoire.fact cimport grm_fact_t, grm_query_t, grm_query_free


cdef extern from "datalog.h":
    ctypedef struct grm_dl_t:
        pass

    int grm_dl_init(grm_dl_t *db) except -1
    int grm_dl_free(grm_dl_t *db) except -1
    int grm_dl_parse(grm_dl_t *db, const char *input, grm_error_t *error)
    grm_query_t *grm_dl_query_parse(grm_dl_t *db, const char *string, grm_error_t *error)
    int grm_dl_fact_to_string(grm_dl_t *db, char *dst, size_t size, const grm_fact_t *src)
    int grm_dl_match_query(grm_dl_t *db,
            grm_fact_t ***response,
            size_t *count,
            grm_query_t *query,
            grm_error_t *error)
    void grm_dl_free_facts(grm_fact_t **facts, size_t count)


cdef class Datalog:
    cdef grm_dl_t *db

    def __cinit__(self):
        self.db = <grm_dl_t*> PyMem_Malloc(sizeof(grm_dl_t))
        grm_dl_init(self.db)

    def __dealloc__(self):
        grm_dl_free(self.db)
        PyMem_Free(self.db)

    def parse(self, dl_str):
        pass
