#!/usr/bin/env python3

from contextlib import contextmanager
from distutils.command.build_clib import build_clib
from distutils.command.clean import clean
from setuptools import setup, Extension
from setuptools.command.test import test
import glob
import os
import subprocess

def run_setup():
    setup(
            name = 'brigadier',
            author = 'Jason M Barnes',
            author_email = 'json.barnes@gmail.com',
            ext_modules = load_extensions(),
            libraries = [('grimoire', {'sources': []})], # purposefully empty
            cmdclass = {
                'build_clib': BuildGrimoire,
                'clean': CleanAllBrigadier,
                'test': TestBrigadier,
            },
            test_suite = 'test',
    )

def use_cython():
    return 'USE_CYTHON' in os.environ

def load_extensions():
    """ Define the pygrimoire extension module.  If Cython is enabled, then
    cythonize the sources.
    """
    source = 'pygrimoire.pyx' if use_cython() else 'pygrimoire.c'
    extensions = [
            Extension('pygrimoire',
                sources = [os.path.join('pygrimoire', source)],
                include_dirs = [os.path.join('grimoire', 'src')],
                library_dirs = [os.path.join('grimoire', 'lib')],
                libraries = ['grimoire']),
    ]
    if use_cython():
        from Cython.Build import cythonize
        extensions = cythonize(extensions)
    return extensions

@contextmanager
def cd(path):
    prevdir = os.getcwd()
    os.chdir(path)
    try:
        yield
    finally:
        os.chdir(prevdir)

class BuildGrimoire(build_clib):
    """ Build the static library from the grimoire submodule. """
    def run(self):
        build_clib.run(self)
        with cd('grimoire'):
            cmd = subprocess.run('make lib/libgrimoire.a'.split())
            assert cmd.returncode == 0

class CleanAllBrigadier(clean):
    """ Clean-up grimoire submodule and any '--inplace' libraries. """
    def run(self):
        clean.run(self)
        if self.all:
            # clean grimoire submodule
            with cd('grimoire'):
                print("cleaning 'grimoire'")
                subprocess.run('make clean'.split())

            # remove any '--inplace' shared libraries
            libs = glob.glob('./*.so')
            if libs:
                for lib in libs:
                    print("removing '%s'" % lib)
                    os.remove(lib)

class TestBrigadier(test):
    """ Run 'build_clib' before 'build_ext --inplace'. """
    def run(self):
        self.run_command('build_clib')
        test.run(self)

if __name__ == '__main__':
    run_setup()
